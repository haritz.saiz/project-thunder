var WebSocketServer = require('websocket').server;
var http = require('http');

module.exports.WebSocketBinder = class WebSocketBinder {
    constructor() {
        this.server = http.createServer(function (request, response) {
            console.log((new Date()) + ' Received request for ' + request.url);
            response.writeHead(404);
            response.end();
        });

        this.server.listen(8080, function () {
            console.log((new Date()) + ' Server is listening on port 8080');
        });

        this.connectionList = []

    }
    newConection(uuid) {
        var that = this
        var wsServer = new WebSocketServer({
            httpServer: this.server,
            autoAcceptConnections: false,
            path: "/" + uuid
        });

        function originIsAllowed(origin) {
            return true;
        }

        wsServer.on('request', function (request) {
            if (!originIsAllowed(request.origin)) {
                // Make sure we only accept requests from an allowed origin
                request.reject();
                console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
                return;
            }

            var connection = request.accept('echo-protocol', request.origin);
            console.log((new Date()) + ' Connection accepted.');
            connection.on('message', function (message) {
                if (message.type === 'utf8') {
                    if (message.utf8Data == "close") {
                        connection.close()
                        wsServer.closeAllConnections()
                    }
                    console.log("websocket-" + uuid);
                    connection.sendUTF(message.utf8Data);
                }
            });
            connection.on('close', function (reasonCode, description) {
                console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
            });

            that.connectionList.push({ uuid: uuid, connection: connection, wsServer: wsServer })
        });
    }

    closeConnection(uuid) {
        var connection = this.connectionList.find(obj => {obj.uuid === uuid ; return obj})
        connection.connection.sendUTF("QR Scanned")
    }
}