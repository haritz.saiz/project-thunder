var WebSocketBinder = require('./binder.js').WebSocketBinder

var express = require("express");
var { v4: uuidv4 } = require('uuid')

var app = express();
app.listen(8000, () => {
    console.log("Server running on port 8000");
});

app.use(express.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var webSocket = new WebSocketBinder()

app.get("/uuid", (req, res, next) => {
    var uuid = uuidv4()
    webSocket.newConection(uuid)
    var resObject = {
        uuid: uuid
    }
    res.json(resObject)
});

app.get("/close-socket/", (req, res, next) => {
    console.log(req.query.userId + " " + req.query.uuid)
    webSocket.closeConnection(req.query.uuid)
    res.json(true)
});