import "lib/datejs/build/date-es-ES.js"

var html = /*html*/`
    <style>
       .wrapper{
            background: black;
            display: flex;
            justify-content: center;
            align-items: flex-start;
            flex-direction: column;
            height: fit-content;
            width: fit-content;
       }
       .big-font{
           font-size: 80px;
       }
       .medium-font{
           font-size: 25px;
           color: #bcbcbc;
       }
    </style>
    <div class="wrapper" id="wrapper">
        <div class="medium-font" id="day"></div>
        <div class="big-font" id="hour">17:25</div>
    </div>
`


export default class DayHour extends HTMLElement {

    constructor() {
        super();
        var that = this;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = html;

        const capitalize = (s) => {
            if (typeof s !== 'string') return ''
            return s.charAt(0).toUpperCase() + s.slice(1)
        }

        var dayElem = this.shadowRoot.getElementById("day"),
        hourElem = this.shadowRoot.getElementById("hour")
        
        setInterval(()=>{
            var today = new Date()
            var dateString = capitalize(today.toString("dddd")) + ", " + capitalize(today.toString("MMMM")) + " " +today.toString("dd")  + ", " + today.toString("yyyy")
            dayElem.textContent = dateString
            hourElem.textContent = today.toString("HH:mm")
        }, 1000)
        
    }

}

customElements.define("day-hour", DayHour)
