var html = /*html*/`
    <style>
       .wrapper{
            background: black;
            display: flex;
            justify-content: center;
            align-items: flex-end;
            flex-direction: column;
            height: fit-content;
            width: fit-content;
       }
       .big-font{
           font-size: 80px;
       }
       .medium-font{
           font-size: 25px;
           color: #bcbcbc;
       }
       .bold{
           font-weight: bold;
       }
       .row{
            justify-content: flex-end;
            align-items: center;
            display: flex;
       }
       ul{
            padding: 0;
       }
       .border-line{
           border-bottom: 2px solid #bcbcbc;
           width: 100%;
           text-align: center;
           height: 25px;
       }
    </style>
    <div class="wrapper" id="wrapper">
        <div class="medium-font">Bilbao</div>
        <div class="big-font row">
            <img src="./img/weather/wi-day-fog.svg" width="80" heigh="100" style="filter:invert(1); margin-right: 20px">
            <span id="temp"></span>
            <span>&deg</span>
        </div>
        <div class="medium-font row">
            <span style="margin-right: 10px">Sensación Termica:</span>
            <span id="feelsLike"></span>
            <span>&deg</span>
        </div>
        <div class="row" style="flex-direction: column; align-items: flex-end; margin-top:50px;">
            <div class="border-line">PREDICCIÓN DEL TIEMPO</div>
            <ul id="weather-list"></ul>
        </div>
    </div>
`


export default class WeatherComponent extends HTMLElement {

    constructor() {
        super();
        var that = this;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = html;

        var units = "metric",
            apiKey = "6adf114143233b3a65cb6180dbbc16e2"
            
        
        var geoSucess = (position)=>{
            var lat = position.coords.latitude ,
            lon = position.coords.longitude    
            fetch("https://api.openweathermap.org/data/2.5/onecall?lat=" + lat + "&lon=" + lon + "&exclude=minutely,daily,current&appid=" + apiKey + "&units=" + units)
                .then(function (response) {
                    return response.json();
                })
                .then(function (json) {
                    console.log(json)

                    var currentTempObject = json.hourly[0]
                    var currentTemp = Number((currentTempObject.temp).toFixed(1));
                    var currentFeelsLike = Number((currentTempObject.feels_like).toFixed(1));
                    var weather = currentTempObject.weather.main

                    that.shadowRoot.getElementById("temp").textContent = currentTemp
                    that.shadowRoot.getElementById("feelsLike").textContent = currentFeelsLike
                });

            fetch("https://api.openweathermap.org/data/2.5/onecall?lat=" + lat + "&lon=" + lon + "&exclude=minutely,hourly,current&appid=" + apiKey + "&units=" + units)
                .then(function (response) {
                    return response.json();
                })
                .then(function (json) {
                    console.log(json)
                    for (let i = 1; i < 6; i++) {
                        const dayTempObject = json.daily[i];
                        var maxTemp = Number((dayTempObject.temp.max).toFixed(1))
                        var minTemp = Number((dayTempObject.temp.min).toFixed(1))
                        var weather = dayTempObject.weather.main
                        var tstamp = dayTempObject.dt

                        var weatherItem = document.createElement("weather-list-item")
                        weatherItem.setAttribute("min", minTemp)
                        weatherItem.setAttribute("max", maxTemp)
                        weatherItem.setAttribute("weather", weather)

                        var date = new Date(tstamp*1000)

                        var weekday = new Array(7);
                        weekday[0] = "Dom";
                        weekday[1] = "Lun";
                        weekday[2] = "Mar";
                        weekday[3] = "Mier";
                        weekday[4] = "Juev";
                        weekday[5] = "Vier";
                        weekday[6] = "Sab";
                        
                        var dayOfWeek = weekday[date.getDay()]
                        weatherItem.setAttribute("tstamp", dayOfWeek)

                        that.shadowRoot.getElementById("weather-list").appendChild(weatherItem)
                    }
                });
        }            

        var geoFail = (er)=>{
            console.log(er)
        }
        navigator.geolocation.getCurrentPosition(geoSucess, geoFail, {enableHighAccuracy: true, timeout: 5000, maximumAge: 0})
    }
        

}

var html2 = /*html*/`
    <style>
        li{
            display: flex;
            justify-content: center; 
            align-items: center;
            width: 250px;
            color: #bcbcbc;
        }
        .margin-item{
            width: 100px;
            margin-right: 25px;
        }
        .margin-item:last-child{
            margin-right: 0px;
        }
        .flex{
            display: flex;
            justify-content: flex-end;
        }
    </style>
    <li>
        <span id="day" class="margin-item">Lunes</span>
        <img id="icon" src="./img/weather/wi-day-fog.svg" class="margin-item" style="filter:invert(1); width: 30px">
        <div class="margin-item flex">
            <span id="max">25.3</span>
            <span>&deg</span>
        </div>
        <div class="margin-item flex">
            <span id="min">25.3</span>
            <span>&deg</span>
        </div>
    </li>
`

class WeatherItem extends HTMLElement {

    constructor() {
        super();
        var that = this;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = html2;
    }

    static get observedAttributes() {
        return ["min", "max", "weather", "tstamp"]
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case "min":
                this.shadowRoot.getElementById("min").textContent = newValue
                break;

            case "max":
                this.shadowRoot.getElementById("max").textContent = newValue
                break;

            case "weather":
                var icon = ""
                switch (newValue) {
                    case "Clouds":
                        icon = "wi-cloudy.svg"
                        break;
                
                    default:
                        icon = "wi-day-fog.svg"
                        break;
                }
                this.shadowRoot.getElementById("icon").textContent = "./img/weather/" + icon
                break;

            case "tstamp":
                this.shadowRoot.getElementById("day").textContent = newValue
                break;
                
            default:
                break;
        }
    }
}

customElements.define("weather-list-item", WeatherItem)
customElements.define("weather-component", WeatherComponent)