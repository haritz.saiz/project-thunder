const path = require('path');

var webpack_config = [
    {
        entry: {
            app: __dirname + "/src/index.js"
        },
        output: {
            path: __dirname + "/build",
            filename: "index.js",
            publicPath: "/build/"
        },
        target: "electron-main",
    },


    {
        entry: {
            app: __dirname + "/src/web-components-loader.js"
        },
        output: {
            path: __dirname + "/build",
            filename: "web-components-loader.js",
            publicPath: "/build/"
        },
        resolve: {
            alias: {
                node_modules: path.resolve(__dirname, "node_modules"),
                lib: path.resolve(__dirname, "./src/lib")
            }
        },
        devServer: {
            host: "0.0.0.0",
            port: "5501",
            inline: true
        },
        target: 'electron-renderer'
    }
];

module.exports = webpack_config;