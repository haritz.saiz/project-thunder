import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:http/http.dart' as http;

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';


class QrScannerScreen extends StatefulWidget {
  @override
  _QrScannerScreenState createState() => _QrScannerScreenState();
}

class _QrScannerScreenState extends State<QrScannerScreen> {

  var qrText = '';
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'QRScanner',
      home: Scaffold(
        appBar: AppBar(
          title: Text('QRScanner'),
        ),
        body: Container(
          height: 700,
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  height: 600,
                  child: QRView(
                    key: qrKey,
                    onQRViewCreated: _onQRViewCreated,
                    overlay: QrScannerOverlayShape(
                      borderColor: Colors.red,
                      borderRadius: 10,
                      borderLength: 10,
                      borderWidth: 10,
                      cutOutSize: 300,
                    ),

                  ),
                ),
                Container(
                  child:  Text('This is the result of scan: $qrText'),
                )
              ],
            ),
        ),
      ),
    )
    );
  }


  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
        controller.pauseCamera();
        dispose();
        http.get('http://35.232.167.89:8000/close-socket?uuid='+qrText+"&userId=haritz");
      });
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}


